﻿/// <summary>
/// For licensing information look for the LICENSE.txt in one of the parent directories.
/// </summary>
namespace RegexBuilder.DL.RegexNodes
{
    /// <summary>
    /// Only RegexNode without a parent so it can act as root node.
    /// </summary>
    public class RegexNodeChainGroup : RegexNodeBase
    {
        /// <summary>
        /// The parent node.
        /// </summary>
        private RegexNodeBase _parent;

        /// <summary>
        /// Gets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public override RegexNodeBase Parent
        {
            get
            {
                return _parent;
            }

            protected set
            {
                _parent = value;
                base.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Parent"));
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether child nodes are allowed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if child nodes are allowed otherwise, <c>false</c>.
        /// </value>
        protected override bool childrenAllowed
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public override bool Equals(RegexNodeBase other)
        {
            return base.Equals(other as RegexNodeChainGroup);
        }
    }
}
