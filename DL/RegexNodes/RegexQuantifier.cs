﻿/// <summary>
/// The corresponding member variable to the Parent property.
/// </summary>
namespace RegexBuilder.DL.RegexNodes
{
    /// <summary>
    /// Structure to define any quantifier in regular expressions.
    /// </summary>
    public struct RegexQuantifier : System.IEquatable<RegexQuantifier>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegexQuantifier"/> struct.
        /// </summary>
        /// <param name="minimum">The minimum.</param>
        /// <param name="maximum">The maximum.</param>
        public RegexQuantifier(uint minimum, uint maximum)
        {
            if (maximum < minimum)
            {
                throw new System.ArgumentOutOfRangeException("maximum");
            }

            Minimum = minimum;
            Maximum = maximum;
        }

        /// <summary>
        /// Gets the minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public uint Minimum
        {
            get;

            private set;
        }

        /// <summary>
        /// Gets the maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public uint Maximum
        {
            get;

            private set;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(RegexQuantifier other)
        {
            return Maximum == other.Maximum && Minimum == other.Minimum;
        }
    }
}
