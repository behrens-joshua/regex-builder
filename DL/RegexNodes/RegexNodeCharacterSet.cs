﻿/// <summary>
/// For licensing information look for the LICENSE.txt in one of the parent directories.
/// </summary>
namespace RegexBuilder.DL.RegexNodes
{
    using System.Collections.Generic;

    /// <summary>
    /// Class to hold any characterset information.
    /// </summary>
    /// <seealso cref="RegexBuilder.DL.RegexNodes.RegexNodeBase" />
    public class RegexNodeCharacterSet : RegexNodeBase
    {
        /// <summary>
        /// The parent node.
        /// </summary>
        private RegexNodeBase _parent;

        /// <summary>
        /// Gets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public override RegexNodeBase Parent
        {
            get
            {
                return _parent;
            }

            protected set
            {
                _parent = value;
                base.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Parent"));
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether child nodes are allowed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if child nodes are allowed otherwise, <c>false</c>.
        /// </value>
        protected override bool childrenAllowed
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// The characterset.
        /// </summary>
        private ISet<char> _characters = new HashSet<char>();

        /// <summary>
        /// Gets or sets the characterset.
        /// </summary>
        /// <value>
        /// The characterset.
        /// </value>
        public ISet<char> Characters
        {
            get
            {
                return _characters;
            }

            set
            {
                _characters = value;
                base.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Characters"));
            }
        }

        /// <summary>
        /// Indicator, whether the characterset is inverted.
        /// </summary>
        private bool _isInverted = false;

        /// <summary>
        /// Gets or sets a value indicating whether this characterset is inverted.
        /// </summary>
        /// <value>
        /// <c>true</c> if this characterset is inverted; otherwise, <c>false</c>.
        /// </value>
        public bool IsInverted
        {
            get
            {
                return _isInverted;
            }

            set
            {
                _isInverted = value;
                base.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("IsInverted"));
            }
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public override bool Equals(RegexNodeBase other)
        {
            RegexNodeCharacterSet tempOther = other as RegexNodeCharacterSet;

            if (base.Equals(tempOther) && IsInverted == tempOther.IsInverted)
            {
                if ((tempOther.Characters == null) == (Characters == null))
                {
                    return Characters == null || Characters.SetEquals(tempOther.Characters);
                }
            }

            return false;
        }
    }
}
