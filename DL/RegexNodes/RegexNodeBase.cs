﻿/// <summary>
/// For licensing information look for the LICENSE.txt in one of the parent directories.
/// </summary>
namespace RegexBuilder.DL.RegexNodes
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Base class for any RegexNode
    /// </summary>
    /// <seealso cref="System.Collections.ObjectModel.ObservableCollection{RegexBuilder.DL.RegexNodes.RegexNodeBase}" />
    public abstract class RegexNodeBase : ObservableCollection<RegexNodeBase>, System.IEquatable<RegexNodeBase>
    {
        /// <summary>
        /// Gets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public abstract RegexNodeBase Parent
        {
            get;

            protected set;
        }

        /// <summary>
        /// Gets or sets the quantification.
        /// </summary>
        /// <value>
        /// The quantification.
        /// </value>
        public RegexQuantifier Quantification
        {
            get;

            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether child nodes are allowed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if child nodes are allowed otherwise, <c>false</c>.
        /// </value>
        protected abstract bool childrenAllowed
        {
            get;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexNodeBase"/> class.
        /// </summary>
        public RegexNodeBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexNodeBase"/> class.
        /// </summary>
        /// <param name="collection">The collection from which the elements are copied.</param>
        public RegexNodeBase(IEnumerable<RegexNodeBase> collection) : base(collection)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexNodeBase"/> class.
        /// </summary>
        /// <param name="list">The list from which the elements are copied.</param>
        public RegexNodeBase(List<RegexNodeBase> list) : base(list)
        {
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            foreach (RegexNodeBase rnb in this)
            {
                rnb.Parent = null;
            }

            base.ClearItems();
        }

        /// <summary>
        /// Inserts an item into the collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert.</param>
        protected override void InsertItem(int index, RegexNodeBase item)
        {
            if (!childrenAllowed)
            {
                throw new System.InvalidOperationException();
            }

            if (item.Parent != null)
            {
                item.Parent.Remove(item);
            }

            item.Parent = this;
            base.InsertItem(index, item);
        }

        /// <summary>
        /// Removes the item at the specified index of the collection.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        protected override void RemoveItem(int index)
        {
            this.ElementAt(index).Parent = null;
            base.RemoveItem(index);
        }

        /// <summary>
        /// Replaces the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to replace.</param>
        /// <param name="item">The new value for the element at the specified index.</param>
        protected override void SetItem(int index, RegexNodeBase item)
        {
            if (!childrenAllowed)
            {
                throw new System.InvalidOperationException();
            }

            item.Parent = this;
            base.SetItem(index, item);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public virtual bool Equals(RegexNodeBase other)
        {
            return other != null && other.Quantification.Equals(Quantification) && other.SequenceEqual(this);
        }
    }
}
